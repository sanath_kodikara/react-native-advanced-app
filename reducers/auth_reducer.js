import { 
  FACEBOOK_LOGIN_SUCCESS, 
  FACEBOOK_LOGIN_FAIL,
  LOGOUT_SUCCESS,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER,
  SIGNUP_USER,
  SIGNUP_USER_SUCCESS,
  SIGNUP_USER_FAIL
} from '../actions/types';

const INITIAL_STATE = { 
  user: null, 
  error: '', 
  loading: false 
};

export default function(state = {}, action) {
  switch(action.type) {
    case FACEBOOK_LOGIN_SUCCESS:
    console.log('facebook login success ', action);
      return { token: action.payload };
    case FACEBOOK_LOGIN_FAIL:
    console.log('facebook login FAILED ', action);
      return { token: null };
    case LOGOUT_SUCCESS:
      return { token: null };
    case LOGIN_USER:
      return { ...state, loading: true, error: ''}  
    case LOGIN_USER_SUCCESS:
      return { ...state, ...INITIAL_STATE ,user: action.payload}  
    case LOGIN_USER_FAIL:
      return { ...state, error: 'Authentication Failed', loading: false}
    case SIGNUP_USER:
      return { ...state, loading: true, error: ''}  
    case SIGNUP_USER_SUCCESS:
      return { ...state, ...INITIAL_STATE ,user: action.payload}  
    case SIGNUP_USER_FAIL:
      return { ...state, error: 'Signup Failed', loading: false} 
    default:
      return state;
  }
}