import { combineReducers } from 'redux';
import auth from './auth_reducer';
import jobs from './jobs_reducer';
import likedJobs from './likes_reducer';
import employeeForm from './employee_form_reducer';
import employee from './employee_reducer';

export default combineReducers({
  auth, jobs, likedJobs, employeeForm, employee
});