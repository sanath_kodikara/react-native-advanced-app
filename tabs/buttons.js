import Expo from 'expo';
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

import { StackNavigator } from 'react-navigation';
import { Icon } from 'react-native-elements';

import ButtonsHome from '../views/buttons_home';
import EmployeeScreen from '../screens/EmployeeForm';
import EmployeeListScreen from '../screens/EmployeeListScreen';
import EmployeeAdd from '../screens/EmployeeAdd';
import EmployeeEdit from '../screens/EmployeeEdit';

const ButtonsTabView = ({ navigation }) => (
  <ButtonsHome navigation={navigation} />
);

const ButtonsTab = StackNavigator({
  employeeList: { 
    screen: EmployeeListScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Employees',
      headerLeft: (
        <Icon
          name="menu"
          size={30}
          type="entypo"
          style={{ paddingLeft: 10 }}
          onPress={() => navigation.navigate('DrawerOpen')}
        />
      ),
    }),
  },
  employee: { screen: EmployeeScreen },
  employeeAdd: { screen: EmployeeAdd },
  employeeEdit: { screen: EmployeeEdit },
});

export default ButtonsTab;
