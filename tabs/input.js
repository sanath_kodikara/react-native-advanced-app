import Expo from 'expo';
import React, { Component } from 'react';

import { StackNavigator } from 'react-navigation';
import { Icon } from 'react-native-elements';

import InputHome from '../views/input_home';
import DeckScreen from '../screens/DeckScreen';

const InputTabView = ({ navigation }) => (
  <InputHome navigation={navigation} />
);

const InputTab = StackNavigator({
  deck: {
    screen: DeckScreen,
    path: '/',
    navigationOptions: ({ navigation }) => ({
      title: 'Jobs',
      headerLeft: (
        <Icon
          name="menu"
          size={30}
          type="entypo"
          style={{ paddingLeft: 10 }}
          onPress={() => navigation.navigate('DrawerOpen')}
        />
      ),
    }),
  }
});

export default InputTab;
