import firebase from 'firebase';
import { AsyncStorage } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { Facebook } from 'expo';
import {
  FACEBOOK_LOGIN_SUCCESS,
  FACEBOOK_LOGIN_FAIL,
  LOGOUT_SUCCESS,
  LOGIN_USER_SUCCESS, 
  LOGIN_USER_FAIL, 
  LOGIN_USER,
  SIGNUP_USER,
  SIGNUP_USER_SUCCESS,
  SIGNUP_USER_FAIL
} from './types';

export const facebookLogin = () => async dispatch => {
  let token = await AsyncStorage.getItem('facebook_token');
  if (token) {
    // Dispatch an action saying FB login completed
    dispatch({ type: FACEBOOK_LOGIN_SUCCESS, payload: token});
  } else {
    // Start up FB login process
    doFacebookLogin(dispatch);
  }
};

export const logout = (callback) => async dispatch => {
  let facebook_token = await AsyncStorage.getItem('facebook_token');
  let firebase_token = await AsyncStorage.getItem('fb_token');
  if (firebase_token) {
    let firebase_token_removed = await AsyncStorage.removeItem('fb_token');
  } else if (facebook_token) {
    let facebook_token_removed = await AsyncStorage.removeItem('facebook_token');
  }
  dispatch({ type: LOGOUT_SUCCESS });
  callback();
};

export const loginUser = ({ email, password }) => {
  return (dispatch) => {
    dispatch({ type: LOGIN_USER });
    firebase.auth().signInWithEmailAndPassword(email, password)
      .then(user => loginUserSuccess(dispatch, user))
      .catch((error) => {
        loginUserFailed(dispatch);
      })
  }
}

export const signupUser = ({ email, password }) => {
  return (dispatch) => {
    dispatch({ type: SIGNUP_USER });
    firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(user => signupUserSuccess(dispatch, user))
        .catch(() => signupUserFailed(dispatch))
  }
}

const doFacebookLogin = async (dispatch) => {
  let { type, token } = await Facebook.logInWithReadPermissionsAsync('852976951570386', {
    permissions: ['public_profile']
  });
  // When login failed
  if (type === 'cancel') {
    return dispatch({ type: FACEBOOK_LOGIN_FAIL })
  }
  //  Save token to Async storage
  await AsyncStorage.setItem('facebook_token', token);
  //Dispatch an action saying FB login completed
  dispatch({ type: FACEBOOK_LOGIN_SUCCESS, payload: token });
};

const loginUserFailed = (dispatch) => {
  dispatch({
    type: LOGIN_USER_FAIL
  });
}

const loginUserSuccess = async (dispatch, user) => {
  //  Save token to Async storage
  await AsyncStorage.setItem('token', user.uid);
  dispatch({
    type: LOGIN_USER_SUCCESS, 
    payload: user
  });
  //dispatch(NavigationActions.navigate({ routeName: 'map' }));
  //key defined in router.js
  //Actions.main();
  console.log('using NavigationActions.navigate ');
  //dispatch(NavigationActions.navigate({ routeName: 'employeeList' }));
  // const navigateAction = NavigationActions.navigate({
  //   routeName: "employeeList",
  // });
  // this.props.navigation.dispatch(navigateAction);
}

const signupUserFailed = (dispatch) => {
  dispatch({
    type: SIGNUP_USER_FAIL
  });
}

const signupUserSuccess = (dispatch, user) => {
  dispatch({
    type: SIGNUP_USER_SUCCESS, 
    payload: user
  });
}
