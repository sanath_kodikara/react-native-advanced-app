import React, { Component } from 'react';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';
import * as actions from '../actions';

class LogoutButton extends Component {
  componentWillMount() {
    this.props.navigation.setParams({
      logout: this.logout.bind(this)
    });
  }

  logout() {
    try {
      this.props.logout(() => {
        this.props.navigation.navigate('auth');
      });
    } catch (error) {
      console.log(error);
    }
  }
  
  render() {
    return (
      <Button
        id={`logout_button-${this.props.id}`} 
        title="Logout" backgroundColor="rgba(0,0,0,0)" color="rgba(0,122,255,1)"
        style={{ paddingRight: 5 }} onPress={() => this.props.navigation.state.params.logout()}
      />
    )
  }
}

export default connect(null, actions)(LogoutButton);