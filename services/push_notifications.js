import { Permissions, Notifications } from 'expo';
import { AsyncStorage } from 'react-native';
import axios from 'axios';

const PUSH_ENDPOINT = 'http://rallycoding.herokuapp.com/api/tokens';

export default async() => {
  let previousToken = await AsyncStorage.getItem('pushtoken');
  console.log('previousToken ----- ',previousToken);
  if (previousToken) {
    return;
  } else {
    //is that ok to send you push notifications
    let { status } = await Permissions.askAsync(Permissions.REMOTE_NOTIFICATIONS)
    
    if(status !== 'granted') {
      return;
    }
    //user has given permission to send push notifications
    
    //generate token
    let token = await Notifications.getExpoPushTokenAsync();

    //save the token to the backend (http://rallycoding.herokuapp.com/api/tokens)
    await axios.post(PUSH_ENDPOINT, { token: { token } });
    
    //save token to async storage
    AsyncStorage.setItem('pushtoken', token);
  }
}