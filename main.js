import React from 'react';
import { Provider } from 'react-redux';
import Expo, { AppLoading, Asset, Font } from 'expo';
import { FontAwesome, Ionicons } from '@expo/vector-icons';
import { View, Image, Dimensions, YellowBox } from 'react-native';
import { Button } from 'react-native-elements';
import { DrawerNavigator, DrawerItems, SafeAreaView } from 'react-navigation';
import firebase from 'firebase';

import store from './store';
import reducers from './reducers';
import Components from './drawer/components';
import Settings from './drawer/settings';
import Lists from './drawer/lists';
import Animations from './drawer/animations';
import CustomDrawerContentComponent from './drawer/customDrawerContent';
import Login from './screens/LoginScreen';

import Config from './config/config.json';

const SCREEN_WIDTH = Dimensions.get('window').width;

const MainRoot = DrawerNavigator(
  {
    Login: {
      path: '/login',
      screen: Login,
    },
    Lists: {
      path: '/lists',
      screen: Lists,
    },
    Components: {
      path: '/components',
      screen: Components,
    },
    Settings: {
      path: '/settings',
      screen: Settings,
    },
    Animations: {
      path: '/animations',
      screen: Animations
    }
  },
  {
    contentComponent: CustomDrawerContentComponent,
    initialRouteName: 'Login',
    contentOptions: {
      activeTintColor: '#548ff7',
      activeBackgroundColor: 'transparent',
      inactiveTintColor: '#ffffff',
      inactiveBackgroundColor: 'transparent',
      labelStyle: {
        fontSize: 15,
        marginLeft: 0,
      },
    },
    drawerWidth: SCREEN_WIDTH * 0.8,
    contentComponent: CustomDrawerContentComponent,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',
  }
);

function cacheImages(images) {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

function cacheFonts(fonts) {
  return fonts.map(font => Font.loadAsync(font));
}

export default class AppContainer extends React.Component {
  state = {
    isReady: false,
  };

  componentWillMount() {
    const { apiKey, authDomain, databaseURL, projectId,
      storageBucket, messagingSenderId } = Config.firebaseConfig;
    const config = {
      apiKey,
      authDomain,
      databaseURL,
      projectId,
      storageBucket,
      messagingSenderId
    };
    firebase.initializeApp(config);
    //YellowBox.ignoreWarnings(['Warning: ReactNative.createElement']);
    console.ignoredYellowBox = ['Warning: ReactNative.createElement'];
  }
  
  componentDidMount() {
    //YellowBox.ignoreWarnings(['Warning: ReactNative.createElement']);
    //console.ignoredYellowBox = ['Warning: ReactNative.createElement'];
  }

  async _loadAssetsAsync() {
    const imageAssets = cacheImages([
      require('./assets/images/bg_screen1.jpg'),
      require('./assets/images/bg_screen2.jpg'),
      require('./assets/images/bg_screen3.jpg'),
      require('./assets/images/bg_screen4.jpg'),
    ]);

    const fontAssets = cacheFonts([FontAwesome.font, Ionicons.font]);

    await Promise.all([...imageAssets, ...fontAssets]);
  }

  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync={this._loadAssetsAsync}
          onFinish={() => this.setState({ isReady: true })}
          // onError={console.warn}
        />
      );
    }

    return (
      <Provider store={store}>
        <MainRoot />
      </Provider>
    );
  }
}

const styles = {
  logoutButton: { 
    flex: 1, 
    backgroundColor: '#43484d',
    paddingRight: 10
  }
}

Expo.registerRootComponent(AppContainer);
