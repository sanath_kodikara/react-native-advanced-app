import { createStore, compose, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import { persistStore, autoRehydrate } from 'redux-persist';
import { AsyncStorage } from 'react-native';
import reducers from '../reducers';

const store = createStore(
  reducers,
  {},
  compose(
    applyMiddleware(reduxThunk),
    //autoRehydrate()
  )
);
//redux persist doesn't work 
//persistStore(store, { storage: AsyncStorage, whitelist: ['likedJobs']})

export default store;