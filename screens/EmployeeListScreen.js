import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, Dimensions, StyleSheet, ScrollView, ListView, Image, Platform, TouchableOpacity } from 'react-native';
import { Avatar, Button, ListItem, Card, Tile, Icon } from 'react-native-elements';
import { Font } from 'expo';
import { NavigationActions } from 'react-navigation';
import { employeesFetch, logout } from '../actions';
import LogoutButton from '../components/LogoutButton';

const SCREEN_WIDTH = Dimensions.get('window').width;

class EmployeeList extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Employees',
    headerStyle: {
      backgroundColor: '#c0d6e4', 
      marginTop: Platform.OS === 'android' ? 24 : 0,
    },
    tabBarIcon: ({ tintColor }) => {
      return <Icon name="emoticon-neutral" size={30} type="material-community" color={tintColor}/>
    },
  });
  
  constructor(props) {
    super(props);

    this.state = {
      fontLoaded: false,
      selectedIndex: 0,
      value: 0.5
    };

    this.updateIndex = this.updateIndex.bind(this);
  }

  updateIndex(selectedIndex) {
    this.setState({ selectedIndex });
  }

  componentWillMount() {
    this.props.employeesFetch();
  }

  async componentDidMount() {
    await Font.loadAsync({
      'georgia': require('../assets/fonts/Georgia.ttf'),
      'regular': require('../assets/fonts/Montserrat-Regular.ttf'),
      'light': require('../assets/fonts/Montserrat-Light.ttf'),
      'bold': require('../assets/fonts/Montserrat-Bold.ttf'),
    });
    this.setState({ fontLoaded: true });
  }

  componentWillReceiveProps(nextProps) {
    // next setof props that this component will render with
    // this.props is still old props
  }

  editEmployee = (emp) => {
    this.props.navigation.navigate('employeeEdit', emp);
  }

  render() {
    const { navigation } = this.props;
    const { selectedIndex } = this.state;

    return (
      <View>
        <ScrollView>
          <View style={styles.list}>
            {this.props.employees.map((l, i) => (
              <ListItem
                leftAvatar={{ rounded: true, source: { uri: l.avatarUrl } }}
                key={i}
                onPress={() => this.editEmployee(l)}
                title={l.name}
                subtitle={l.phone}
                chevron
                bottomDivider
              />
            ))}
          </View>
        </ScrollView>
        <TouchableOpacity
          style={styles.floatingButton}
          onPress={() => this.props.navigation.navigate('employeeAdd')}>
          <Icon name="plus-box-outline" size={30} type="material-community" color='#fff'/>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    marginTop: 20,
    borderTopWidth: 1,
    borderColor: '#e3e3e3',
    backgroundColor: '#fff',
  },
  headerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 40,
    backgroundColor: '#FD6B78',
  },
  heading: {
    color: 'white',
    marginTop: 10,
    fontSize: 22,
  },
  user: {
    flexDirection: 'row',
    marginBottom: 6,
  },
  image: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  name: {
    fontSize: 16,
    marginTop: 5,
  },
  social: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  subtitleView: {
    flexDirection: 'row',
    paddingLeft: 10,
    paddingTop: 5,
  },
  buttonStyle: {
    paddingRight: 6,
  },
  floatingButton: {
    borderWidth:1,
    borderColor:'rgba(0,0,0,0.2)',
    backgroundColor:'rgba(0,122,255,1)',
    alignItems:'center',
    justifyContent:'center',
    width:70,
    position: 'absolute',                                          
    bottom: 10,                                                    
    right: 10,
    height:70,
    borderRadius:100,
  }
});

const mapStateToProps = state => {
  console.log('mapstatetoprops ..... ', state);
  //convert to an array
  const employees = _.map(state.employee, (val, uid) => {
    return { ...val, uid }
  });
  return { employees };
}

export default connect(mapStateToProps, { employeesFetch, logout })(EmployeeList);