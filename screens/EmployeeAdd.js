import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Platform } from 'react-native';
import { employeeUpdate, employeeCreate } from '../actions';
import { Avatar, Button, ListItem, Tile, Icon } from 'react-native-elements';
import { Card, CardSection } from '../components/common';
import EmployeeForm from './EmployeeForm';

class EmployeeCreate extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerStyle: {
      backgroundColor: '#c0d6e4', 
      marginTop: Platform.OS === 'android' ? 24 : 0,
    },
    title: 'Employees',
    tabBarIcon: ({ tintColor }) => {
      return <Icon name="emoticon-neutral" size={30} type="material-community" color={tintColor}/>
    },
  });

  onButtonPress() {
    const { name, phone, shift, avatarUrl } = this.props;
    this.props.employeeCreate({ name, phone, shift: shift || 'Monday', avatarUrl });
  }

  render() {
    
    return (
      <Card>
        <EmployeeForm {...this.props} />
        <View style={{ padding: 10 }}>
          <Button title="Create Employee" onPress={this.onButtonPress.bind(this)} />
        </View> 
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  const { name, phone, shift, avatarUrl } = state.employeeForm;
  return { name, phone, shift, avatarUrl };
}

export default connect(mapStateToProps, { 
  employeeUpdate, employeeCreate })(EmployeeCreate);