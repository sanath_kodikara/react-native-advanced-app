import React, { Component } from 'react';
import { View, Text, ActivityIndicator, Platform } from 'react-native';
import { Button, Icon } from 'react-native-elements'; 
import { MapView } from 'expo';
import { connect } from 'react-redux';
import * as actions from '../actions';
import LogoutButton from '../components/LogoutButton';

class MapScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Map',
    headerLeft: null,
    headerStyle: {
      backgroundColor: '#c0d6e4', 
      marginTop: Platform.OS === 'android' ? 24 : 0,
    },
    tabBarIcon: ({ tintColor }) => {
      return <Icon name="my-location" size={30} color={tintColor}/>
    },
  });

  state = {
    mapLoaded: false,
    region: {
      longitude:-122,
      latitude: 37,
      longitudeDelta: 0.04,
      latitudeDelta: 0.09
    },
  }

  componentDidMount() {
    this.setState({ mapLoaded: true });
  }
  
  onRegionChangeComplete = (region) => {
    this.setState({ region });
  }

  onButtonPress = () => {
    this.props.fetchJobs(this.state.region, () => {
      this.props.navigation.navigate('deck');
    });
  }

  render() {
    if (!this.state.mapLoaded) {
      return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    return (
      <View style={{ flex: 1}}>
        <MapView
          region={this.state.region}
          style={{ flex: 1 }}
          onRegionChangeComplete={this.onRegionChangeComplete}
        />
        <View style={styles.buttonContainer}>
          <Button 
            large
            title="Search this area"
            backgroundColor="#009688"
            icon={
                <Icon
                    name='search'
                    size={15}
                    color='white'
                />
              }
            onPress={this.onButtonPress}
          />
        </View>
      </View>
    )
  }
}

const styles = {
  buttonContainer: {
    position: 'absolute',
    bottom: 20,
    left: 20,
    right: 20
  }
}

export default connect(null, actions)(MapScreen);