import React, { Component } from 'react';
import { Picker, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { employeeUpdate } from '../actions';
import { Input } from 'react-native-elements'

class EmployeeForm extends Component {
  render() {
    return (
      <View>
        <View style={{ padding: 5 }}>
          <Input 
            label="Name"
            placeholder="Jane"
            value={this.props.name}
            onChangeText={value => this.props.employeeUpdate({prop: 'name', value})}
          />
        </View>

        <View style={{ padding: 5 }}>
          <Input 
            label="Phone"
            placeholder="555-555-5555"
            value={this.props.phone}
            onChangeText={value => this.props.employeeUpdate({prop: 'phone', value})}
          />
        </View>

        <View style={{ padding: 5 }}>
          <Input 
            label="Avatar URL"
            placeholder="https://image_ulr/image.jpg"
            value={this.props.avatarUrl}
            onChangeText={value => this.props.employeeUpdate({prop: 'avatarUrl', value})}
          />
        </View>

        <View style={{ flexDirection: 'row', padding: 5 }}>
          <Text style={styles.pickerTextStyle}>Shift</Text>
          <Picker
            style={{ flex: 1, left: -20 }}
            selectedValue={this.props.shift}
            onValueChange={value => this.props.employeeUpdate({prop: 'shift', value})}
          >
            <Picker.Item label="Monday" value="Monday" />
            <Picker.Item label="Tuesday" value="Tuesday" />
            <Picker.Item label="Wednesday" value="Wednesday" />
            <Picker.Item label="Thursday" value="Thursday" />
            <Picker.Item label="Friday" value="Friday" />
            <Picker.Item label="Saturday" value="Saturday" />
            <Picker.Item label="Sunday" value="Sunday" />
          </Picker>
        </View>
      </View>
    );
  }
}

const styles = {
  pickerTextStyle: {
    fontSize: 16,
    paddingLeft: 0,
    paddingTop: 3,
    color: '#86939e',
    fontWeight: 'bold',
  }
}

const mapStateToProps = (state) => {
  const { name, phone, shift, avatarUrl } = state.employeeForm;
  return { name, phone, shift, avatarUrl };
}

export default connect(mapStateToProps, { employeeUpdate })(EmployeeForm);