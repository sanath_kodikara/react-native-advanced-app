import _ from 'lodash';
import React, { Component } from 'react';
import { Picker, Text, View, Platform } from 'react-native';
import { connect } from 'react-redux';
import { Avatar, Button, ListItem, Tile, Icon } from 'react-native-elements';
import Communications from 'react-native-communications';
import { employeeUpdate, employeeSave, employeeDelete } from '../actions';
import EmployeeForm from './EmployeeForm';
import { CardSection, Card, Confirm } from '../components/common';

class EmployeeEdit extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Employees',
    headerStyle: {
      backgroundColor: '#c0d6e4', 
      marginTop: Platform.OS === 'android' ? 24 : 0,
    },
    tabBarIcon: ({ tintColor }) => {
      return <Icon name="emoticon-neutral" size={30} type="material-community" color={tintColor}/>
    },
  });

  state = { showModal: false };

  componentWillMount() {
    const { params } = this.props.navigation.state;
    _.each(params, (value, prop) => {
      this.props.employeeUpdate({ prop, value });
    });
  }

  onButtonPress() {
    const { name, phone, shift, avatarUrl } = this.props;
    this.props.employeeSave({ name, phone, shift, uid: this.props.navigation.state.params.uid, avatarUrl });
  }

  onTextPress() {
    const { phone, shift } = this.props;
    Communications.textWithoutEncoding(phone, `Your upcoming shift is on ${shift}`);
  }

  onAccept() {
    const { uid } = this.props.employee;
    this.props.employeeDelete({ uid });
  }

  onDecline() {
    this.setState({ showModal: false });
  }

  render() {
    return (
      <Card>
        <EmployeeForm />
        <View style={{ padding: 10 }}>
          <Button title="Save Changes" onPress={ this.onButtonPress.bind(this)} />
        </View>
        <View style={{ padding: 10 }}>
          <Button title="Text Schedule" onPress={ this.onTextPress.bind(this)} />
        </View>
        <View style={{ padding: 10 }}>
          <Button title="Fire Employee" onPress={() => this.setState({ showModal: !this.state.showModal }) } />
        </View>
        <Confirm 
          visible={this.state.showModal}
          onAccept={this.onAccept.bind(this)}
          onDecline={this.onDecline.bind(this)}
        >
          Are you sure you want to delete this?
        </Confirm>
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  const { name, phone, shift, avatarUrl } = state.employeeForm;
  return { name, phone, shift, avatarUrl };
}

export default connect(mapStateToProps, { employeeUpdate, employeeSave, employeeDelete })(EmployeeEdit);