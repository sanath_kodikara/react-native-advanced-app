import React, { Component } from 'react';
import { View, Text, Platform } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { clearLikedJobs } from '../actions';

class SettingsScreen extends Component {
  static navigationOptions = {
    title: 'Settings',
    headerStyle: {
      backgroundColor: '#c0d6e4', 
      marginTop: Platform.OS === 'android' ? 24 : 0,
    },
    headerTitleStyle: {
      fontWeight: 'bold',
    },
    tabBarIcon: ({ tintColor }) => {
      return <Icon name="settings" size={30} color={tintColor}/>
    },
  }
  render() {
    return (
      <View>
        <Button 
          title="Reset liked jobs"
          largeicon={{ name: 'delete-forever' }}
          backgroundColor="#F44336"
          onPress={this.props.clearLikedJobs}
          style={{ paddingTop: 20, paddingLeft: 20, paddingRight: 20 }}
        />
      </View>
    )
  }
}

export default connect(null, { clearLikedJobs }) (SettingsScreen);