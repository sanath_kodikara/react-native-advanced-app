import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import Magnetometer from '../components/Magnetometer';
import Pedometer from '../components/Pedometer';
import Location from '../components/Location';
import Gyroscope from '../components/Gyroscope';

export default class MagnetometerSensor extends React.Component {
  render() {

    return (
      <View style={styles.container}>
        <View style={styles.deviceContainer}>
          <Text style={styles.header}>Magnetometer</Text>
          <View><Magnetometer /></View>
        </View>
        <View style={styles.deviceContainer}>
          <Text style={styles.header}>Pedometer</Text>
          <View><Pedometer /></View>
        </View>
        <View style={styles.deviceContainer}>
          <Text style={styles.header}>Location</Text>
          <View><Location /></View>
        </View>
        <View style={styles.deviceContainer}>
          <Text style={styles.header}>Gyroscope</Text>
          <View><Gyroscope /></View>
        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  deviceContainer: {
    padding: 10
  },
  header: {
    fontSize: 18,
  },
  buttonContainer: {
    flexDirection: 'row',
    alignItems: 'stretch',
    marginTop: 15,
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee',
    padding: 10,
  },
  middleButton: {
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderColor: '#ccc',
  },
  sensor: {
    marginTop: 15,
    paddingHorizontal: 10,
  },
});