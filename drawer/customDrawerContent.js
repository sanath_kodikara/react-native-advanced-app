import React, { Component } from 'react';
import { DrawerItems, SafeAreaView } from 'react-navigation';
import { View } from 'react-native';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';
import * as actions from '../actions';

class CustomDrawerContentComponent extends Component {
  componentWillMount() {
    this.props.navigation.setParams({
      logout: this.logout.bind(this)
    });
  }

  logout() {
    try {
      this.props.logout(() => {
        this.props.navigation.navigate('Login');
      });
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <SafeAreaView style={styles.logoutButton}>
        <View style={{ marginLeft: 10 }}>
          <DrawerItems {...this.props} />
          <Button title="Logout" onPress={() => this.props.navigation.state.params.logout()}/>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = {
  logoutButton: { 
    flex: 1, 
    backgroundColor: '#43484d',
    paddingRight: 10
  }
}

export default connect(null, actions)(CustomDrawerContentComponent);