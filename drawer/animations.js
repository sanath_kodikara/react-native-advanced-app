import Expo from 'expo';
import React from 'react';
import { StackNavigator } from 'react-navigation';
import { Icon } from 'react-native-elements';

import Animations from '../views/animations';

const AnimationsDrawerItem = StackNavigator({
  Animations: {
    screen: Animations,
    navigationOptions: ({ navigation }) => ({
      title: 'Animations',
      headerStyle: {
        borderBottomWidth: 0,
        backgroundColor: '#f5f5f5',
      },
      headerLeft: (
        <Icon
          name="menu"
          size={30}
          type="entypo"
          iconStyle={{ paddingLeft: 10 }}
          onPress={() => navigation.navigate('DrawerOpen')}
        />
      ),
    }),
  },
});

AnimationsDrawerItem.navigationOptions = {
  drawerLabel: 'Animations',
  drawerIcon: ({ tintColor }) => (
    <Icon
      name="cog"
      size={30}
      iconStyle={{
        width: 30,
        height: 30,
      }}
      type="entypo"
      color={tintColor}
    />
  ),
};

export default AnimationsDrawerItem;
